package com.example.harun.bloodbank;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.fragments.FragmentLogin;
import com.example.harun.bloodbank.fragments.FragmentRegister;
import com.example.harun.bloodbank.fragments.FragmentRememberPassword;
import com.example.harun.bloodbank.interfaces.LoginInterface;

public class Login extends AppCompatActivity implements LoginInterface , FragmentManager.OnBackStackChangedListener {
    TextView registerTV;
    FragmentManager manager;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FragmentLogin fl = new FragmentLogin();
        manager = getSupportFragmentManager();
        manager.addOnBackStackChangedListener(this);
        transaction = manager.beginTransaction();
        transaction.add(R.id.container,fl,"fLogin");
        transaction.addToBackStack("fragLogin");
        transaction.commit();


    }

    @Override
    public void goRegister() {
        FragmentRegister fr = new FragmentRegister();
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.container,fr,"fRegister");
        transaction.addToBackStack("fragRegister");
        transaction.commit();
    }

    @Override
    public void goRememberPassword() {
        FragmentRememberPassword frp = new FragmentRememberPassword();
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.container,frp,"fRPW");
        transaction.addToBackStack("fragRPW");
        transaction.commit();
    }

    @Override
    public void goMainPage() {
        Intent intent = new Intent(Login.this,MainPage.class);
        startActivity(intent);
    }

    @Override
    public void goLogin() {
        FragmentLogin fl = new FragmentLogin();
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();

        transaction.replace(R.id.container,fl,"fLogin");
        transaction.addToBackStack("fragLogin");

        transaction.commit();

    }



    @Override
    public void onBackStackChanged() {

    }
}
